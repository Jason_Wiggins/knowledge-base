#Bash Reference Guide

Bash (BourneAgainShell) is a shell language built on-top of the original Bourne Shell
which was distributed with V7 Unix in1979 and became the standard for writing
shell scripts.

Today it is primary to most Linux distributions, MacOS and it has even recently
been enabled to run on Windows through something called WSL(Windows Subsystem for Linux).    


** **    


**File Test Operators**    
Testing ﬁles in scripts is easy and straight forward.  This is where shell scripting starts to show its glory!    
In Bash you can do ﬁle testing for permissions, size, date, ﬁle type or mere existence.    

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<th>Flag</th>
<th>Description</th>
</tr>
<tr class="even">
<td>-e</td>
<td>File exists</td>
</tr>
<tr class="odd">
<td>-a</td>
<td>File exists (identical to -e but is deprecated and outdated)</td>
</tr>
<tr class="even">
<td>-f</td>
<td>File is a regular ﬁle (not a directory or device ﬁle)</td>
</tr>
<tr class="odd">
<td>-s</td>
<td>ﬁle is not zero size</td>
</tr>
<tr class="even">
<td>-d</td>
<td>ﬁle is a directory</td>
</tr>
<tr class="odd">
<td>-b</td>
<td>ﬁle is a block device</td>
</tr>
<tr class="even">
<td>-c</td>
<td>ﬁle is a character device</td>
</tr>
<tr class="odd">
<td>-p</td>
<td>ﬁle is a pipe</td>
</tr>
<tr class="even">
<td>-h</td>
<td>ﬁle is a symbolic link</td>
</tr>
<tr class="odd">
<td>-L</td>
<td>ﬁle is a symbolic link</td>
</tr>
<tr class="even">
<td>-S</td>
<td>ﬁle is a socket</td>
</tr>
<tr class="odd">
<td>-t</td>
<td>ﬁle (descriptor) is associated with a terminal device; this test option may be used to check whether the stdin [ -t 0 ] or stdout [ -t 1 ] in a given script is a terminal</td>
</tr>
<tr class="even">
<td>-r</td>
<td>ﬁle has read permission (for the user running the test)</td>
</tr>
<tr class="odd">
<td>-w</td>
<td>ﬁle has write permission (for the user running the test)</td>
</tr>
<tr class="even">
<td>-x</td>
<td>ﬁle has execute permission (for the user running the test)</td>
</tr>
<tr class="odd">
<td>-g</td>
<td>set-group-id (sgid) ﬂag set on ﬁle or directory</td>
</tr>
<tr class="even">
<td>-u</td>
<td>set-user-id (suid) ﬂag set on ﬁle.</td>
</tr>
<tr class="odd">
<td>-k</td>
<td>sticky bit set.</td>
</tr>
<tr class="even">
<td>-O</td>
<td>you are owner of ﬁle</td>
</tr>
<tr class="odd">
<td>-G</td>
<td>group-id of ﬁle same as yours</td>
</tr>
<tr class="even">
<td>-N</td>
<td>ﬁle modiﬁed since it was last read</td>
</tr>
<tr class="odd">
<td>f1 - nt f2</td>
<td>ﬁle f1 is newer than f2</td>
</tr>
<tr class="even">
<td>f1 -ot f2</td>
<td>ﬁle f1 is older than f2</td>
</tr>
<tr class="odd">
<td>f1 -ef f2</td>
<td>ﬁles f1 and f2 are hard links to the same ﬁle</td>
</tr>
<tr class="even">
<td>!</td>
<td>Not – reverses the sense of the tests above (returns true if condition absent).</td>
</tr>
<tr class="odd">
</tr>
</tbody>
</table>

&nbsp;
****

**Integer Comparison Operators**    
How to compare integers or arithmetic expressions in shell scripts.    

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<th>Flag</th>
<th>Description</th>
</tr>
<tr class="even">
<td>-eq</td>
<td>is equal to</td>
</tr>
<tr class="odd">
<td>-ne</td>
<td>is not equal to</td>
</tr>
<tr class="even">
<td>-gt</td>
<td>is greater than</td>
</tr>
<tr class="odd">
<td>-gt</td>
<td>is greater than</td>
</tr>
<tr class="even">
<td>-ge</td>
<td>is greater than or equal to</td>
</tr>
<tr class="odd">
<td>lt</td>
<td>is less than</td>
</tr>
<tr class="even">
<td>le</td>
<td>is less than or equal to</td>
</tr>
<tr class="odd">
<td><</td>
<td>is less than – place within double parentheses</td>
</tr>
<tr class="even">
<td><=</td>
<td>is less than or equal to (same rule as previous row)</td>
</tr>
<tr class="odd">
<td>></td>
<td>is greater than (same rule as previous row)</td>
</tr>
<tr class="even">
<td>>=</td>
<td>is greater than or equal to (same rule as previous row)</td>
</tr>
</tbody>
</table>

&nbsp;
****

**String Comparison Operators**    
String comparison in Bash.    

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<th>Flag</th>
<th>Description</th>
</tr>
<tr class="even">
<td>=</td>
<td>is equal to</td>
</tr>
<tr class="odd">
<td>==</td>
<td>the same as above</td>
</tr>
<tr class="even">
<td>!=</td>
<td>is not equal to</td>
</tr>
<tr class="odd">
<td><</td>
<td>is less than ASCII alphabetical order</td>
</tr>
<tr class="even">
<td>></td>
<td>is greater than ASCII alphabetical order</td>
</tr>
<tr class="odd">
<td>-z</td>
<td>string is null (i.e. zero length)</td>
</tr>
<tr class="even">
<td>-n</td>
<td>string is not null (i.e. !zero length)</td>
</tr></tbody></table>   

&nbsp;
****

**Compound Operators**    
Useful for boolean expressions and is similar to **&&** and **||**.    
The compound operators work with the test command or may occur within single brackets **[ <expr\> ]**.    

<table>
<colgroup>
<col style="width: 5%"/>
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<th>Flag</th>
<th>Description</th>
</tr>
<tr>
<td>-a</td>
<td>logical and</td>
</tr>
<tr class="odd">
<td>-o</td>
<td>logical or</td>
</tr></tbody></table>   

&nbsp;
****

**Job Identiﬁers**    
Job control allows you to selectively stop (suspend) the execution of processes and continue their execution at a later point in time.    

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<th>Notation</th>
<th>Description</th>
</tr>
<tr class="even">
<td>%N</td>
<td>job number [N]</td>
</tr>
<tr class="odd">
<td>%S</td>
<td>Invocation (command-line) of job begins with string S</td>
</tr>
<tr class="even">
<td>%?S</td>
<td>Invocation (command-line) of job contains within it string S</td>
</tr>
<tr class="odd">
<td>%%</td>
<td>" current" job (last job stopped in foreground or started in background)</td>
</tr>
<tr class="even">
<td>%+</td>
<td>" current" job (last job stopped in foreground or started in background)</td>
</tr>
<tr class="odd">
<td>%-</td>
<td>last job</td>
</tr>
<tr class="even">
<td>%!</td>
<td>last background process</td>
</tr></tbody></table>  

&nbsp;
****

**List Constructs**    
Provides a means of processing commands consecutively and ineffect is able to replace complex if/then/case structures.    

<table>
<colgroup>
<col style="width: 5%"/>
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<th>Construct</th>
<th>Description</th>
</tr>
<tr>
<td>&&</td>
<td>and construct</td>
</tr>
<tr class="odd">
<td>||</td>
<td>or construct</td>
</tr></tbody></table>

&nbsp;
***

**Reserved Exit Codes**    
Useful for debugging a script.    
Exit takes integer args in the range 0-255.    

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<th>Exit Code No.</th>
<th>Description</th>
</tr>
<tr class="even">
<td>1</td>
<td>Catchall for general errors</td>
</tr>
<tr class="odd">
<td>2</td>
<td>Misuse of shell builtins</td>
</tr>
<tr class="even">
<td>126</td>
<td>Command invoked cannot execute</td>
</tr>
<tr class="odd">
<td>127</td>
<td>Command not found</td>
</tr>
<tr class="even">
<td>128</td>
<td>Invalid argument to exit</td>
</tr>
<tr class="odd">
<td>128+n</td>
<td>Fatal error signal " n"</td>
</tr>
<tr class="even">
<td>130</td>
<td>Script terminated by Control-C</td>
</tr></tbody></table>

&nbsp;
***

**Signals**    
UNIX System V Signals.    

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 5%" />
<col style="width: 10%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<th>Exit Code No.</th>
<th>Number</th>
<th>Action</th>
<th>Description</th>
</tr>
<tr class="even">
<td>SIGHUP</td>
<td>1</td>
<td>exit</td>
<td>Hangs up</td>
</tr>
<tr class="odd">
<td>SIGINT</td>
<td>2</td>
<td>exit</td>
<td>Interrupts</td>
</tr>
<tr class="even">
<td>SIGQUIT</td>
<td>3</td>
<td>core dump</td>
<td>Quits</td>
</tr>
<tr class="odd">
<td>SIGILL</td>
<td>4</td>
<td>core dump</td>
<td>Illegal instruction</td>
</tr>
<tr class="even">
<td>SIGTRAP</td>
<td>5</td>
<td>core dump</td>
<td>Trace trap</td>
</tr>
<td>SIGIOT</td>
<td>6</td>
<td>core dump</td>
<td>IOT instruction</td>
</tr>
<tr class="even">
<td>SIGEMT</td>
<td>7</td>
<td>core dump</td>
<td>MT instruction</td>
</tr>
<tr class="odd">
<td>SIGFPE</td>
<td>8</td>
<td>core dump</td>
<td>Floating point exception</td>
</tr>
<tr class="even">
<td>SIGKILL</td>
<td>9</td>
<td>exit</td>
<td>Kills (cannot be caught or ignored)</td>
</tr>
<tr class="odd">
<td>SIGBUS</td>
<td>10</td>
<td>core dump</td>
<td>Bus error</td>
</tr>
<tr class="even">
<td>SIGSEGV</td>
<td>11</td>
<td>core dump</td>
<td>Segmentation violation</td>
</tr>
<tr class="odd">
<td>SIGSYS</td>
<td>12</td>
<td>core dump</td>
<td>Bad argument to system call</td>
</tr>
<tr class="even">
<td>SIGPIPE</td>
<td>13</td>
<td>exit</td>
<td>Writes on a pipe with no one to read it</td>
</tr>
<td>SIGALRM</td>
<td>14</td>
<td>exit</td>
<td>Alarm clock</td>
</tr>
<tr class="even">
<td>SIGTERM</td>
<td>15</td>
<td>exit</td>
<td>Software termination signal</td>
</tr>
</tbody></table>

&nbsp;
***

**Sending Control Signals**    
You can use these key-combinations to send signals    

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<th>Key Combo</th>
<th>Description</th>
</tr>
<tr class="even">
<td>Ctrl+C</td>
<td>The interrupt signal, sends SIGINT to the job running in the foreground.</td>
</tr>
<tr class="odd">
<td>Ctrl+Y</td>
<td>The delayed suspend character. Causes a running process to be stopped when it attempts to read input from the terminal. Control is returned to the shell, the user can foreground, background or kill the process. Delayed suspend is only available on operating systems supporting this feature.</td>
</tr>
<tr class="even">
<td>Ctrl+Z</td>
<td>The suspend signal, sends a SIGTSTP to a running program, thus stopping it and returning control to the shell.</td>
</tr></tbody></table>

> Check your stty settings. **Suspend** and **resume** of output is usually disabled if you are using "modern" terminal emulations.   
> The standard xterm supports **Ctrl+S** and **Ctrl+Q** by default.

&nbsp;
***

**File Types**    
This is very different from Windows but straight forward once you get it.    

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<th>Symbol</th>
<th>Meaning</th>
</tr>
<tr class="even">
<td>-</td>
<td>regular file</td>
</tr>
<tr class="odd">
<td>d</td>
<td>directory</td>
</tr>
<tr class="even">
<td>l</td>
<td>symbolic link</td>
</tr>
<tr class="odd">
<td>c</td>
<td>character device</td>
</tr>
<tr class="even">
<td>s</td>
<td>socket</td>
</tr>
<tr class="odd">
<td>p</td>
<td>named pipe</td>
</tr>
<tr class="even">
<td>b</td>
<td>block device</td>
</tr>
</tbody>
</table>

&nbsp;
***

**Permissions**    
Now you may know what that string rwxrwxrwx is when you invoke 'ls'    

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<th>Code</th>
<th>Description</th>
</tr>
<tr class="even">
<td>s</td>
<td>setuid when in user column</td>
</tr>
<tr class="odd">
<td>s</td>
<td>setgid when in group column</td>
</tr>
<tr class="even">
<td>t</td>
<td>sticky bit</td>
</tr>
<tr class="odd">
<td>0 or -</td>
<td>The access right that is supposed to be in this place is not granted.</td>
</tr>
<tr class="even">
<td>4 or r</td>
<td>read access is granted to the user category defined in this place</td>
</tr>
<tr class="odd">
<td>2 or w</td>
<td>write permission is granted to the user category defined in this place</td>
</tr>
<tr class="even">
<td>1 or x</td>
<td>execute permission is granted to the user category defined in this place</td>
</tr>
<tr class="odd">
<td>u</td>
<td>user permissions.</td>
</tr>
<tr class="even">
<td>g</td>
<td>group permissions</td>
</tr>
<tr class="odd">
<td>o</td>
<td>others permissions</td>
</tr>
</tbody>
</table>

&nbsp;
***

**Special Files**    
Files that are read by the shell.    
Listed in order of their execution.    

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<th>File</th>
<th>Info</th>
</tr>
<tr class="even">
<td>/etc/profile</td>
<td>Executed automatically at login</td>
</tr>
<tr class="odd">
<td>~.bash_profile<br />~/.bash_login<br />~.profile</td>
<td>Whichever is found first is executed at login</td>
</tr>
<tr class="even">
<td>~/.bashrc</td>
<td>Is read by every nonlogin shell</td>
</tr>
</tbody>
</table>

&nbsp;
***

**Quoting**    
The following text shows characters that need to be quoted if you want to use their literal symbols and not their special meaning.    

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 45%" />
</colgroup>
<tbody>
<tr class="odd">
<th>Symbol</th>
<th>Literal Meaning</th>
</tr>
<tr class="even">
<td>;</td>
<td>Command seperator</td>
</tr>
<tr class="odd">
<td>&</td>
<td>Background execution</td>
</tr>
<tr class="even">
<td>()</td>
<td>Command grouping</td>
</tr>
<tr class="odd">
<td>|</td>
<td>Pipe</tr>
<tr class="even">
<td>< > &</td>
<td>Redirection symbols</td>
</tr>
<tr class="odd">
<td>* ? [ ] ~ + - @ !</td>
<td>Filename metacharacters</td>
</tr>
<tr class="even">
<td>" ' \</td>
<td>Used in quoting characters</td>
</tr>
<tr class="odd">
<td>$</td>
<td>Variable, command or arithmetic substituion</td>
</tr>
<tr class="even">
<td>#</td>
<td>Start a command that ends on a linebreak</td>
</tr>
<tr class="odd">
<td>space, tab, newline</td>
<td>Word seperators</td>
</tr>
</tbody>
</table>

>
* Everything between "..." is taken literally, except $ (dollar) \`(backtick) and "(double-quotation).    
* Everything between '...' is taken literally, except '(single-quote).    
* The characters following \ is taken literally. Use \ to escape anything in " ..." or '...'    
* Using $ before "..." or '...' causes some special behavior. $" ..." is the same as "..." except that locale translation is done. Likewise, $'...' is similar to $'...' except that the quoted string is processed for escape sequences.    

&nbsp;
***
